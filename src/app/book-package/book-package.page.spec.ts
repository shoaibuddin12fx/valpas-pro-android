import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookPackagePage } from './book-package.page';

describe('BookPackagePage', () => {
  let component: BookPackagePage;
  let fixture: ComponentFixture<BookPackagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookPackagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookPackagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
