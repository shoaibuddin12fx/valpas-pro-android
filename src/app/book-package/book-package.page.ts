import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../services/common.service';
import { BookstoreService } from '../services/bookstore.service';
import { AlertController } from '@ionic/angular';
import { StoragelocationService } from '../services/storagelocation.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-book-package',
  templateUrl: './book-package.page.html',
  styleUrls: ['./book-package.page.scss'],
  providers:[StoragelocationService]
})
export class BookPackagePage implements OnInit {

  user: FormGroup;
  tempArray: any = [];
  storageArea: any;
  fromQRCode: any;
  
  constructor(public formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private commonService: CommonService,
    private bookstoreService: BookstoreService,
    private storagelocationService: StoragelocationService,
    private alertCtrl: AlertController,
    private router: Router) {

      this.user = this.formBuilder.group({
        area: ['', Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(1000), Validators.pattern('^[A-Z a-z 0-9]*$')])],
      });

      this.commonService.packageData.subscribe(result => {
        this.tempArray = result;
      });
  
      this.commonService.fromQRCode.subscribe(result => {
        this.fromQRCode = result;
      });
  
      this.storagelocationService.getStorageAreas().subscribe(data=>{
        this.storageArea = data.responseData.entities;
      });

    }
    
  ngOnInit() {
    setTimeout(() => {
      if(undefined != localStorage.getItem(environment.PACKAGE_STORAGE)){
        this.user.controls['area'].setValue(parseInt(localStorage.getItem(environment.PACKAGE_STORAGE)));
        this.user.controls['name'].setValue(localStorage.getItem(environment.PACKAGE_COMMENT));
      }
    }, 400);
  }
 
  async onSubmit(formData) {
    localStorage.setItem(environment.PACKAGE_STORAGE, formData.value.area);
    localStorage.setItem(environment.PACKAGE_COMMENT, formData.value.name);
    this.bookstoreService.insertPackages(formData, this.tempArray).subscribe(async res => {
      
      if(null != this.fromQRCode && this.fromQRCode.length >0){
        let alert = await this.alertCtrl.create({
          message: 'Booking done',
          buttons: [{
            text: 'DONE',
            handler: data => {
                this.router.navigateByUrl("home");
            }
          },{
            text: 'SCAN MORE',
            handler: data => {
                this.router.navigateByUrl("readqrcode");
            }
          }
        ]
        });
        alert.present();
      } else {
        let alert = await this.alertCtrl.create({
          message: 'Booking done',
          buttons: [{
            text: 'HOME',
            handler: data => {
              this.router.navigateByUrl("package-list");
            }
          }]
        });
        alert.present();
      }
    }, async error => {
      let alert = await this.alertCtrl.create({
        message: 'Error Occured',
        buttons: [{
          text: 'OK',
          handler: data => {
            if(this.fromQRCode){
              this.router.navigateByUrl("home");
            } else {
              this.router.navigateByUrl("package-list");
            }
          }
        }]
      });
      alert.present();

    });
  }

}
