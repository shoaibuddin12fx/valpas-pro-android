import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookPackagePageRoutingModule } from './book-package-routing.module';

import { BookPackagePage } from './book-package.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    BookPackagePageRoutingModule
  ],
  declarations: [BookPackagePage]
})
export class BookPackagePageModule {}
