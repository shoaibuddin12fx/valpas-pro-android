import { TestBed } from '@angular/core/testing';

import { ProjecttagService } from './projecttag.service';

describe('ProjecttagService', () => {
  let service: ProjecttagService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjecttagService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
