import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { TranslationService } from './translation-service.service';
import { UserService } from './user.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class BobjectsService {

  constructor(
    public users: UserService, 
    public utility: UtilityService, 
    public translation: TranslationService,
    public storage: StorageService) { 

      
    }

    prepareObject(): Promise<any>{

      return new Promise( async resolve => {
        const coords = await this.utility.checkLocations();
        const locale = await this.translation.getLocale();
        const token = await this.users.getUserToken();
        const user = await this.users.getUserData();
        console.log(coords);
  
        const obj = {
          "username": user ? user['username'] : null,
          "id": user ? user['userId'] : null,
          "accountId": user ? user['accountId'] : null,
          "refreshToken": user ? user['refreshToken'] : null,
          "token": token,
          "locale": locale,
          "timestamp": Math.floor(Date.now()/1000),
          "latitude":coords['lat'],
          "longitude":coords['lng'],
          "isMobile":true
        };
        
        resolve(obj);
      })
  
    }
}
