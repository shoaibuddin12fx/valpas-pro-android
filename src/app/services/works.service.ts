import { Injectable } from '@angular/core';
import { BobjectsService } from './bobjects.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import * as _ from 'underscore';

@Injectable({
  providedIn: 'root'
})
export class WorksService {

  public item;
  public storages = [];
  public selectors = [];
  constructor(public network: NetworkService, public bobject: BobjectsService, public sqlite: SqliteService, public utility: UtilityService) { }

  setItem(item){
    return new Promise( resolve => {
      this.item = item;
      localStorage.setItem('witem', JSON.stringify(item));
      resolve(true);
    })
  }

  getPropertyItemById(id){

    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      const obj = {
        id: id
      }

      const merge = { ...data, ...obj };


      // data['value'] = id; 
      
      this.network.getPropertyItemById(merge).then( async v => {
        console.log(v); 

        let orders = v['propertyHistoryViewModels'].sort((a, b) => (parseInt(a.externalId) > parseInt(b.externalId) ) ? -1 : 1)        

        resolve({
          info: v['infoText'],
          plist: orders
        })
      })
    })

  }

  getItem(){
    return this.item;
  }

  getStorages(){
    return new Promise( async resolve => {
      console.log('getStorages', this.storages);
      if(this.storages.length > 0){
        resolve(this.storages);
      }else{
        console.log('getStorages', this.storages);
        let data = await this.bobject.prepareObject();
        console.log(data);
        this.network.getStorages(data).then( v => {
          console.log(v);
          if(v.entities){
            this.storages = v.entities;
          }
          resolve(this.storages);
        })
      }
      
    })
    
  }

  getStorageSelectorsByNetwork(){

    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      const obj = {
        // value: material.name
      }

      const merge = {...obj, ...data};


      // data['value'] = id; 
      
      this.network.getStoragesById(merge).then( async v => {
        console.log(v.stockitems);        

        // await this.sqlite.setStocksInDatabase(v.stockitems)
        // this.selectors = v.stockitems ? v.stockitems : [];
        
        resolve(v.stockitems)
      })
    })

  }

  getStorageSelectorById(material){

    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      const obj = {
        // value: material.name
      }

      const merge = {...obj, ...data};


      // data['value'] = id; 
      
      this.network.getStoragesById(merge).then( async v => {
        console.log(v.stockitems);

        await this.sqlite.setStocksInDatabase(v.stockitems)
        // this.selectors = v.stockitems ? v.stockitems : [];
        
        resolve(true)
      })
    })

  }

  setOrderStorage(entity){
    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      data['entity'] = entity;

      console.log(data);

      this.network.getAddMaterielLine(data).then( v => {
        console.log(v);
        resolve(v);
      })

      


    })
  }

  setNewOrderStorage(entity){
    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      data['entity'] = entity;

      console.log(data);

      this.network.getAddMaterielLine(data).then( v => {
        console.log(v);
        resolve(v);
      })

      


    })
  }

  setOrderComplete(entity){
    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      data['entity'] = entity;

      console.log(data);

      this.network.postCompleteOrder(data).then( v => {
        console.log(v);
        resolve(v);
      })

      


    })
  }

  getOrderLinesByOrderId(orderId){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();
      data['ownerId'] = orderId;
      delete data['id'];

      console.log(data);
      
      this.network.getOrderLines(data).then( v => {
        console.log("order lines", v);
        resolve(v.entities)
      })
    })
  }

  getOrderTypesByOrderId(orderId){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();
      // data['ownerId'] = orderId;
      delete data['id'];

      console.log(data);
      
      this.network.getOrderTypes(data).then( v => {
        console.log("order lines", v);
        resolve(v.entities)
      })
    })
  }

  removeOrderLinesByOrderId(orderId){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();
      data['ids'] = [orderId];
      delete data['id'];

      console.log(data);
      
      this.network.removeOrderLines(data).then( v => {
        console.log("order lines", v);
        resolve(v)
      })
    })
  }

  getCostGroup(){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();      
      
      this.network.getCostGroup(data).then( v => {
        console.log("getCostGroup", v);
        resolve(v.entities)
      })
    })
  }

  getWorkTypes(){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();      
      
      this.network.getWorkTypes(data).then( v => {
        console.log("getWorkTypes", v);
        resolve(v.entities)
      })
    })
  }

  getWorkStatus(){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();      
      
      this.network.getWorkStatus(data).then( v => {
        console.log("getWorkStatus", v);
        resolve(v.entities)
      })
    })
  }

  getLocationQuantity(item){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();      
      let obj = {
        id: item.id,
        value: item.location
      }

      const merge = {...data, ...obj};
      
      this.network.getLocationQuantity(merge).then( v => {
        console.log("getLocationQuantity", v);
        resolve(v)
      })
    })
    
  }

  getAllCustomers(){

    return new Promise( async resolve => {

      var bobj = await this.bobject.prepareObject();
      bobj['id'] = null;
      const merged = {...bobj}

      this.network.getAllCustomers(merged).then( async v => {
        if(v){
          if(v['buildings']){
            resolve(v['buildings']);
            return;
          }
        }
        
        resolve([]);
      })

    })

  }

  getCustomerById(id){

    return new Promise( async resolve => {

      this.sqlite.getBuildingsItemById(id).then( async v => {
        resolve(v);
      })

    })

  }

  getServiceListByCustomerId(item){ 

    return new Promise( async resolve => {

      var bobj = await this.bobject.prepareObject();
      bobj['buildingId'] = item['id']; // id;
      const merged = {...bobj}

      this.network.getServiceListByCustomerId(merged).then( async v => {
        if(v){
          if(v['entities']){

            // group by here for data
            var grouped = _.chain(v['entities']).groupBy("frequency").value();            
            console.log('dang',{grouped});
            resolve(grouped);
            return;
          }
        }
        
        resolve([]);
      })

    })

  }

  postCompleteServiceTask(obj){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();   
      const merge = {...data, ...obj}
      
      this.network.postCompleteServiceTask(merge).then( v => {
        console.log("postCompleteServiceTask", v);
        resolve(v)
      })
    })
  }

  postSendMeterReading(obj){
    return new Promise( async resolve => {
      
      var data = await this.bobject.prepareObject();  
      data['entity'] = obj; 
      //const merge = {...data, ...obj}
      
      this.network.postSendMeterReading(data).then( v => {
        console.log("postSendMeterReading", v);
        resolve(v)
      })
    })
  }

  setTaskComment(entity){
    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      const merge = {...data, ...entity}

      this.network.setTaskComment(merge).then( v => {
        console.log(v);
        resolve(v);
      })

      


    })
  }

  postQuickCompleteTask(entity){
    return new Promise( async resolve => {

      var data = await this.bobject.prepareObject();
      const merge = {...data, ...entity}

      this.network.postQuickCompleteTask(merge).then( v => {
        console.log(v);
        resolve(v);
      })

      


    })
  }




}
