import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeNotesPageRoutingModule } from './time-notes-routing.module';

import { TimeNotesPage } from './time-notes.page';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeNotesPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [TimeNotesPage]
})
export class TimeNotesPageModule {}
