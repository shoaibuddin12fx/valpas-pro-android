import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QrOutPage } from './qr-out.page';

describe('QrOutPage', () => {
  let component: QrOutPage;
  let fixture: ComponentFixture<QrOutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrOutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QrOutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
