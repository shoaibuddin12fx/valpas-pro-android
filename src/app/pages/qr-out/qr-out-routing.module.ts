import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QrOutPage } from './qr-out.page';

const routes: Routes = [
  {
    path: '',
    component: QrOutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrOutPageRoutingModule {}
