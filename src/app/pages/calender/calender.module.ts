import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalenderPageRoutingModule } from './calender-routing.module';

import { CalenderPage } from './calender.page';
import { NgCalendarModule } from 'ionic2-calendar';
import { TranslateModule } from '@ngx-translate/core';
import { MonthviewComponent } from './monthview/monthview.component';
import { WorksDetailPage } from '../works/works-detail/works-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalenderPageRoutingModule,
    TranslateModule,
    NgCalendarModule
  ],
  declarations: [CalenderPage, MonthviewComponent, WorksDetailPage]
})
export class CalenderPageModule {}
