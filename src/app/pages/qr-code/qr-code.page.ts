import { Component, Injector, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { QrcodeService } from 'src/app/services/qrcode.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.page.html',
  styleUrls: ['./qr-code.page.scss'],
})
export class QrCodePage extends BasePage implements OnInit {

  elementType = 'url';
  qrcode_src = 'NOSCAN';
  user;
  projectData;

  isScanSuccess = false;

  codeIn = null;
  
  constructor(injector: Injector, 
    public barcodeScanner: BarcodeScanner,
    public qrcodeService: QrcodeService) { 
    super(injector);
    this.initialize()
  }

  ngOnInit() {

  }

  async initialize(){
    this.user = await this.users.getUserData();
    this.openScan();
    
  }



  async openScan(){

    let value = await this.barcodeScan();
    this.qrcode_src = value;
    const obj = await this.qrcodeService.prepareQrcodeObject(value);
    this.callNetworkResponse(obj)

  }

  async qrcodeIn(){
    const obj = await this.qrcodeService.prepareQrcodeObject(this.qrcode_src, true);
    const data = await this.callNetworkResponse(obj);
    this.nav.pop();
  }

  async qrcodeOut() {
    const obj = await this.qrcodeService.prepareQrcodeObject(this.qrcode_src, false);
    const data = await this.callNetworkResponse(obj);
    if(data){
      this.nav.pop();
    }
    
  }

  callNetworkResponse(obj): Promise<any>{

    return new Promise( resolve => {
      if(this.projectData){
        obj["comments"] = this.projectData["comments"];
      }
      
      this.network.getItsQrcode(obj).then( res => {
        this.isScanSuccess = true;
        if(res){
          this.projectData = res;
          this.projectData["comments"] = "";
          
        }

        resolve(this.projectData);
  
      }, err => {
        this.isScanSuccess = true;
        resolve(null)
        
      })
    })
    
  }



  barcodeScan(): Promise<any>{
    return new Promise( async resolve => {
      
      if(this.platform.is('cordova')){
        this.barcodeScanner.scan().then(barcodeData => {
          resolve(barcodeData.text);
        })
      }else{
        resolve(7565);
      }
      
    })
  }



}
