import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.page.html',
  styleUrls: ['./splash-screen.page.scss'],
})
export class SplashScreenPage extends BasePage implements OnInit {

  splash = true;
  messages = 'loading';
  link = "https://nopsa.testi.loginets.com/kmls/jumping-pin.gif";
  constructor(injector: Injector, 
    private menu: MenuController,
    private androidPermissions: AndroidPermissions) { 
    super(injector);
    
    this.platform.ready().then(() => {
      // this.splashScreen.hide();
      this.menu.swipeGesture(false);
    });
    
    // this.initialize()
  }

  ionViewWillEnter() { 
    // setTimeout(() => this.splash = false, 4000);
    this.initialize()
  }

  ngOnInit() {

  }

  async initialize(){

    // function timeout(ms) {
    //   return new Promise(resolve => setTimeout(resolve, ms));
    // }

    // var permissions = this.platform.is('cordova') ? await this.checkPermissions() : await this.checkGeolocation();
    
    // var [args] = await Promise.all([
    //   permissions,
    //   timeout(500)
    // ]);

    // this.splash = false;
    // if(!permissions){
    //   this.messages = 'please_turn_on_location_permission'
    // }
    // else{
    //   this.events.publish('user:get');
    // }

    
  }

  checkGeolocation():Promise<any> {

    return new Promise(async resolve => {

      const coords = await this.utility.getCurrentLocationCoordinates();
      // console.log(coords);


      resolve(coords)
    })
    
  }

  checkPermissions(){
    return new Promise((resolve) => {
      resolve(this.geolocation.checkGPSPermission());
    })
  }

}
