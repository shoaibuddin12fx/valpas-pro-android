import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WorkTimePage } from './work-time.page';

describe('WorkTimePage', () => {
  let component: WorkTimePage;
  let fixture: ComponentFixture<WorkTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkTimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WorkTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
