import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalenderEventDetailPageRoutingModule } from './calender-event-detail-routing.module';

import { CalenderEventDetailPage } from './calender-event-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalenderEventDetailPageRoutingModule
  ],
  declarations: [CalenderEventDetailPage]
})
export class CalenderEventDetailPageModule {}
