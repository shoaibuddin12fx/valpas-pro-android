import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalenderEventDetailPage } from './calender-event-detail.page';

describe('CalenderEventDetailPage', () => {
  let component: CalenderEventDetailPage;
  let fixture: ComponentFixture<CalenderEventDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderEventDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CalenderEventDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
