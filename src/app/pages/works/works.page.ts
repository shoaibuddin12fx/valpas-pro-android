import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';
import { WorksDetailPage } from './works-detail/works-detail.page';
import { IonBackButtonDelegate } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-works',
  templateUrl: './works.page.html',
  styleUrls: ['./works.page.scss'],
})
export class WorksPage extends BasePage implements OnInit, OnDestroy {

  plist: any[] = [];
  public item;
  public type;
  public myTotalWork = 0;
  subscription: Subscription;  
  public showMeters = false;

  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;

  constructor(injector: Injector, public bobject: BobjectsService) { 
    super(injector);

    let url = location.href;
    let splits = url.split('works/')
    this.type = splits[1][0];    
    console.log(this.type);  
    
    if(this.type == 'c'){
      this.showMeters = localStorage.getItem('meters') == 'true';
      console.log(this.showMeters);
    }
    


  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.subscription = null;
  }

  ngOnInit() {
    console.log('ionViewDidEnter');

    
    if(!this.subscription){
      this.subscription = this.platform.backButton.subscribeWithPriority(10, () => {
        let f = localStorage.getItem('backBtnSub');
        if(f == 'true'){
          this.setUIBackButtonAction();
        }
        
      })
    }
  }

  

  setUIBackButtonAction() {
    this.back()
  }

  async back(){
    let url = location.href;
    let splits = url.split('works/')
    let customer_id = splits[1].split('/')[0].replace('c', '');

    if(customer_id){
      console.log('propertyInOut OUT')
      
      this.nav.pop().then( v => {

        if(this.subscription){
          let f = localStorage.getItem('backBtnSub');
          if(f == 'true'){
            localStorage.setItem('backBtnSub', 'false');
            // check back log from services             
            this.setPropertyInOut("OUT")            
          }
          
        }
        
      });
    }else{
      this.nav.setRoot('/home');
    }
    
  }

  getItem(){
    return this.item;
  }

  getWorkList(data): Promise<any[]>{

    var self = this;
    return new Promise( async resolve => {

      const bobj = await this.bobject.prepareObject();
      const merged = {...bobj, ...data}
      console.log(merged)

      this.network.getAllWorks(merged).then( async v => {
        console.log(v);
        if(v){
          if(v['orders']){
            let orders = v['orders'].sort((a, b) => (parseInt(a.externalId) > parseInt(b.externalId) ) ? -1 : 1)
            resolve(orders);
            return;
          }
        }
        
        resolve([]);
      })

    })

  }

  

  getMyWorkList(data): Promise<any[]>{

    var self = this;
    return new Promise( async resolve => {

      const bobj = await this.bobject.prepareObject();
      var merged = {...bobj, ...data}
      console.log(merged)

      this.network.getAllMyWorks(merged).then( async v => {
        console.log(v);
        if(v){
          if(v['orders']){

            let orders = v['orders'].sort((a, b) => (parseInt(a.externalId) > parseInt(b.externalId) ) ? -1 : 1)
            self.myTotalWork = v['orders'].length;
            console.log(self.myTotalWork )
            resolve(orders);
            return;
          }
        }
        
        resolve([]);
      })

    })

  }

  async selectWork(item){
    // open modal 
    //const data = await this.modals.present(WorksDetailPage, {item: item});
    await this.works.setItem(item);
    const data = this.nav.push('works-detail')

    
  }

  getTotalWork(){
    return this.myTotalWork;
  }

  // "propertyId":,
  // "buildingId":,
  // "inOut": "IN" | "OUT"
  // only get IN OUT and extract customer from localstorage
  // 
  setPropertyInOut(inOut): Promise<any[]>{

    let customer = localStorage.getItem("customer") ? JSON.parse(localStorage.getItem("customer")) : null;

    var self = this;
    return new Promise( async resolve => {

      if(!customer){
        resolve([]);
        return;
      }

      let data = {
        "propertyId":customer["propertyId"],
        "buildingId":customer["id"],
        "inOut": inOut
      };

      const bobj = await this.bobject.prepareObject();
      var merged = {...bobj, ...data}
      console.log(merged)

      this.network.setPropertyInOut(merged).then( async v => {
        console.log(v);
        
        resolve([]);
      })

    })

  }







}
