import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompleteWorkPageRoutingModule } from './complete-work-routing.module';

import { CompleteWorkPage } from './complete-work.page';
import { TranslateModule } from '@ngx-translate/core';
import { WorkClassesComponent } from './work-classes/work-classes.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { WorkCommentComponent } from './work-comment/work-comment.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CompleteWorkPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [CompleteWorkPage, WorkClassesComponent, WorkCommentComponent ]
})
export class CompleteWorkPageModule {}
