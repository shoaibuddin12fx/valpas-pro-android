import { AfterViewInit, Component, Injector, Input, OnInit } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../../base-page/base-page';
import { FiltersComponent } from '../other-work/filters/filters.component';
import { WorkHistoryComponent } from '../work-history/work-history.component';
import { WorkMetersComponent } from '../work-meters/work-meters.component';
import { WorkMenuComponent } from '../works-detail/work-menu/work-menu.component';
import { WorksPage } from '../works.page';
import * as _ from 'underscore';

@Component({
  selector: 'app-work-service',
  templateUrl: './work-service.component.html',
  styleUrls: ['./work-service.component.scss'],
})
export class WorkServiceComponent extends WorksPage implements OnInit, AfterViewInit {


  services = [];
  plist = [];
  filters: any = {
    "fromDate": 1577864643000,
    "toDate": 1600078767000,
    "unallocated": false,
    "unallocatedOnly": true,
    "statusCode": null,
    "orderTypeIds": [],
    "costGroups": []
  };

  costs = [];
  statuses = [];
  types = [];
  plist_WEEKLY_open = true;
  plist_MONTHLY_open = true;
  plist_YEARLY_open = true;
  plist_MANDATORY_open = false;

  constructor(injector: Injector, public bobject: BobjectsService) {
    super(injector, bobject);
    this.events.subscribe("reload:worklist", this.initialize.bind(this))
  }

  ngAfterViewInit(): void {
    // call api 
    let customer = JSON.parse(localStorage.getItem("customer"))
  }

  ngOnInit() { 
    // console.log('ionViewDidEnter');
    // localStorage.setItem('backBtnSub', 'true');
  }

  initialize() {
    return new Promise(async resolve => {

      // update date 
      var startDate = new Date();
      startDate.setMonth(startDate.getMonth() - 10);
      let sd = Math.round(startDate.getTime())

      var endDate = new Date();
      endDate.setMonth(endDate.getMonth() + 2);
      let ed = Math.round(endDate.getTime())

      this.filters['fromDate'] = sd;
      this.filters['toDate'] = ed;

      if (this.type == 'w') {

        // month and year as int 
        // "year" and "month" p
        let date = new Date();
        this.filters['month'] = date.getMonth();
        this.filters['year'] = date.getFullYear();


      }

      

      this.customer = JSON.parse(localStorage.getItem("customer"))
      let item = {
        id: this.customer.id
      };
      this.plist = await this.works.getServiceListByCustomerId(item) as [];
      console.log('receiveed: ',this.plist);

      this.plist_WEEKLY_open = true;
      this.plist_MONTHLY_open = true;
      this.plist_YEARLY_open = true;
      this.plist_MANDATORY_open = false;



      this.getFilters();

    })

  }

  ionViewWillEnter() {

    let url = location.href;
    let splits = url.split('works/')
    console.log("is it", splits[1][0]);
    this.type = splits[1][0];
    console.log(this.type);


    this.initialize();
  }

  getServicebookList(data): Promise<any[]> {


    return new Promise(async resolve => {

      const bobj = await this.bobject.prepareObject();
      var merged = { ...bobj, ...data }
      delete merged['id']
      console.log(merged)

      this.network.getServicebookList(merged).then(async v => {
        console.log(v);
        if (v) {
          if (v['orders']) {
            resolve(v['orders']);
            return;
          }
        }

        resolve([]);
      })

    })

  }

  async completeItem(obj) {

    obj.complete = obj.complete == true ? false : true ;
    let data = {
      id: obj.id,
      orderId: obj.orderId,
      description: obj.description,
      buildingId: localStorage.getItem("customerBuildingId")
    }

    // const dta = await this.works.postCompleteServiceTask(data);
    // console.log(dta);



  }

  async moreMenu($event) {
    console.log("open meun here", this.customer);
    const _data = await this.popover.present(WorkMenuComponent, $event, { flag: this.type == 'w' ? 'UNW' : 'UNWC', pid: 0 });
    const data = _data.data;
    console.log(data);

    let flag = data.param;

    if (flag == 'M') {
      this.openMeterList();
    }

    if (flag == 'S') {
      this.openWorkFilters();
    }

    if (flag == 'I') {
      this.openPropertyInfo();
    }

  }

  async openWorkFilters() {
    let _data = await this.modals.present(FiltersComponent, { filters: this.filters, costs: this.costs, statuses: this.statuses, types: this.types }, 'small-modal');
    let data = _data.data;

    if (data) {
      console.log(data);
      if (data.data != 'A') {
        this.filters = data;
        this.plist = await this.getWorkList(this.filters);
      }
    }
  }

  async openMeterList() {

    let customer = JSON.parse(localStorage.getItem("customer"))
    console.log(customer)
    let _data = await this.modals.present(WorkMetersComponent, { item: customer });
    let data = _data.data;

  }

  async openPropertyInfo() {

    let customer = JSON.parse(localStorage.getItem("customer"))
    console.log(customer)
    let _data = await this.modals.present(WorkHistoryComponent, { item: customer });
    let data = _data.data;

  }

  async getFilters() {

    this.costs = await this.works.getCostGroup() as [];
    this.statuses = await this.works.getWorkStatus() as [];
    this.types = await this.works.getWorkTypes() as [];

    const st = this.statuses.find(x => x.default == true);
    if (st) {
      this.filters.statusCode = st.id;
    }

  }

  async completeTasks($event){


    await this.setPropertyInOut("OUT")

    let customer = JSON.parse(localStorage.getItem("customer"))

    let plistIds_WEEKLY = this.plist['WEEKLY'].filter( x => {
      return x.complete == true 
    }).map( x => x.id);

    let plistIds_MONTHLY = this.plist['MONTHLY'].filter( x => {
      return x.complete == true 
    }).map( x => x.id);

    let plistIds_YEARLY = this.plist['YEARLY'].filter( x => {
      return x.complete == true 
    }).map( x => x.id);

    let plistIds_MANDARORY = this.plist['MANDATORY'].filter( x => {
      return x.complete == true 
    }).map( x => x.id);

    let plistIds = [
      ...plistIds_WEEKLY,
      ...plistIds_MONTHLY,
      ...plistIds_YEARLY,
      ...plistIds_MANDARORY
    ];
    
    let data = {
      "ownerId": customer.id, 
      "ids":plistIds
    }


    await this.works.postQuickCompleteTask(data);
    this.translation.getTranslateKey('info_data_update_success').then( v => {
      this.utility.presentSuccessToast(v)
    })
    this.nav.pop().then( v => {
      this.nav.pop();
    })
  }



}
