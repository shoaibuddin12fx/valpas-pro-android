import { Component, Injector, Input, OnInit } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../../base-page/base-page';
import { WorkMeterDetailComponent } from '../work-meters/work-meter-detail/work-meter-detail.component';
import { WorksDetailPage } from '../works-detail/works-detail.page';

@Component({
  selector: 'app-work-history',
  templateUrl: './work-history.component.html',
  styleUrls: ['./work-history.component.scss'],
})
export class WorkHistoryComponent extends BasePage implements OnInit {

  plist = [];
  info;
  _item: boolean;
  get item(): boolean {
      return this._item;
  }
  @Input() set item(value: boolean) {
      this._item = value;
      console.log(value);
      
  }
  
  constructor(injector: Injector, public bobject: BobjectsService) { 
    super(injector);
  }

  ngAfterViewInit(): void {
    // call api 
    this.getPropertyItemById();
    
  }

  ngOnInit() {}

  async getPropertyItemById(){

    const data = await this.works.getPropertyItemById(this.item['propertyId']);
    console.log(data);
    this.plist = data['plist'];
    this.info = data['info'];

  }

  back(){
    this.modals.dismiss();
  }

  

  async selectWork(item){
    // open modal 
    await this.works.setItem(item);
    const data = await this.modals.present(WorksDetailPage, {asHistory: true, item: item});
    
    // const data = this.nav.push('works-detail')

    
  }

}

