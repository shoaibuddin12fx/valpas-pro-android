import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectorSearchPage } from './selector-search.page';

const routes: Routes = [
  {
    path: '',
    component: SelectorSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectorSearchPageRoutingModule {}
