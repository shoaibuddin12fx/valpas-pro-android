import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkGalleryComponent } from './work-gallery.component';
import { WorkGalleryPageRoutingModule } from './work-gallery-routing.module';
import { FileListComponent } from 'src/app/pages/works/works-detail/components/file-list/file-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkGalleryPageRoutingModule,
    TranslateModule,
  ],
  declarations: [WorkGalleryComponent,FileListComponent],
  providers: [ DatePipe ] 
})
export class WorkGalleryPageModule {}
