import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkMaterialComponent } from './work-material/work-material.component';

import { WorksDetailPage } from './works-detail.page';

const routes: Routes = [
  {
    path: '',
    component: WorksDetailPage
  },
  {
    path: 'material',
    component: WorkMaterialComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorksDetailPageRoutingModule {}
