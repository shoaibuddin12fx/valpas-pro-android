import { EventEmitter, Component, Input, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss'],
})
export class FileListComponent implements OnInit {


  _item: any;
  _userDetails:any;
  params:any;
  path:any;
  checkbox_hidden:boolean = true;
  get item(): any {
    return this._item;
  }
  get userDetails():any{
    return this._userDetails;
  }
  @Input() set item(value: any) {
    this._item = value;

    // call image details api here in this component 
    // and get the image and show inside any image tag in html for preview
  }

  @Input() set userDetails(value:any){
    this._userDetails = value;
  }

  @Output('openImage') openImage: EventEmitter<any> = new EventEmitter<any>();
  @Output('openFile') openFile: EventEmitter<any> = new EventEmitter<any>();

  constructor(public network:NetworkService,public events:EventsService) { 
    // this.events.subscribe('file uploaded',this.getFilePath.bind(this))
    this.events.subscribe('checkbox_hidden',()=>{
      this.checkbox_hidden = !this.checkbox_hidden;
    })
  }

  ngOnInit() { 
    this.getFilePath();
  }

  getFilePath(){
    this.params = {...this.userDetails,...this.item}
    console.log(this.userDetails);
    console.log(this.item);
    this.network.getImageDetail(this.params).then(path =>{
      this.path = path.responseData;
    })
  }

  sendPictureClickEvent(path){
    console.log('event published')
    this.events.publish('Picture Clicked',path);
  }

  emitOpenFile(path){
    console.log(path);
    if(this.checkbox_hidden == true){
      this.openFile.emit(path)
    }
  }
}
