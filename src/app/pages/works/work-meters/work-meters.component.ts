import { AfterViewInit, Component, Injector, Input, OnInit } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../../base-page/base-page';
import { WorkMeterDetailComponent } from './work-meter-detail/work-meter-detail.component';

@Component({
  selector: 'app-work-meters',
  templateUrl: './work-meters.component.html',
  styleUrls: ['./work-meters.component.scss'],
})
export class WorkMetersComponent  extends BasePage implements OnInit, AfterViewInit {

  type: any;
  plist = [];
  _item: boolean;
  get item(): boolean {
      return this._item;
  }
  @Input() set item(value: boolean) {
      this._item = value;
      console.log(value);
      
  }


  constructor(injector: Injector, public bobject: BobjectsService) { 
    super(injector);
  }

  ngAfterViewInit(): void {
    // call api 
    this.item = JSON.parse(localStorage.getItem("customer"))
    this.getMetersList();
    
  }

  ngOnInit() {

    let url = location.href;
    let splits = url.split('works/')
    console.log("is it", splits[1][0]);
    this.type = splits[1][0];    
    console.log(this.type);  

  }

  getMetersList(){

    return new Promise( async resolve => {
      const bobj = await this.bobject.prepareObject();
      var merged = {...bobj}

      if(this.item){ 
        let obj = {
          id: this.item['id'],
        }
        merged = {...bobj, ...obj}
      }

      console.log(merged)

      this.network.getMetersData(merged).then( v => {
        console.log(v);
        this.plist = v.entities;
        // this.events.publish("reload:worklist")
      })
    })

  }

  back(){

    this.nav.pop();
    // if(this.type == 'c'){
    //   this.nav.pop();
    // }else{
    //   this.modals.dismiss()
    // }

  }

  async openDetails(item){
    const _data = await this.modals.present(WorkMeterDetailComponent, {item}, 'small-modal');
    this.getMetersList();
  }

}
