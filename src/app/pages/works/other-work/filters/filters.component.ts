import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent extends BasePage implements OnInit {

  @Input('filters') filters;

  @Input('costs') costs = [];
  @Input('statuses') statuses = [];
  @Input('types') types = [];

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  save(){
    this.modals.dismiss(this.filters);
  }

  back(){
    this.modals.dismiss({data: 'A'});
  }

  close(res){
    this.modals.dismiss(res);
  }

}
