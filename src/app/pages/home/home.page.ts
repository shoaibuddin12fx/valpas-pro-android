import { AppSettings } from './../../services/app-settings';
import { Component, Injector } from '@angular/core';
import { HomeService } from './../../services/home-service';
import { ModalController } from '@ionic/angular';
import { IntroPage } from '../intro-page/intro-page.page';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [HomeService]
})
export class HomePage extends BasePage {

  item;
  
  constructor( injector: Injector,
    private homeService:HomeService, 
    public modalController: ModalController) { 
      super(injector);
      this.events.publish('user:init_sidemenu');
      
      this.item = this.homeService.getData();
      let showWizard = localStorage.getItem("SHOW_START_WIZARD");

      if (AppSettings.SHOW_START_WIZARD && !showWizard) {
        this.openModal()
      }

      this.initialize();
  }

  async initialize(){
    
    var time = localStorage.getItem('stocks_call');
    if(!time){
      var ts = Math.round((new Date()).getTime() / 1000).toString();
      localStorage.setItem('stocks_call', ts );
      this.setupStorage();
      return;
    }

    var ntime = parseInt(localStorage.getItem('stocks_call'));
    
    if( ( (ntime % 1) / 3600) > 1 ){
      this.setupStorage()
    }

    
  }

  async setupStorage(){
    
    this.utility.showLoader();
    let list = await this.works.getStorageSelectorsByNetwork();    
    console.log(list);
    await this.sqlite.setStocksInDatabase(list)

    let customers = await this.works.getAllCustomers();    
    console.log(customers);
    await this.sqlite.setBuildingsInDatabase(customers)
    this.utility.hideLoader();

  }

  async openModal() {
    let modal = await this.modalController.create({component: IntroPage});
     return await modal.present();
  }
}
