import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SettingService } from '../services/setting.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProjectService } from '../services/project.service';
import { StatuscodeService } from '../services/statuscode.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
  providers: [SettingService, ProjectService, StatuscodeService]
})
export class SettingPage implements OnInit {

  user: FormGroup;
  projectTags: any;
  stausCodes: any;
  arr: any = [];

  constructor(public formBuilder: FormBuilder,
    public settingService: SettingService,
    public projectService: ProjectService,
    public statuscodeService: StatuscodeService,
    private alertCtrl: AlertController,
    private router: Router) { 

      this.user = this.formBuilder.group({
        project: ['', Validators.compose([Validators.required])],
        statuscode: ['', Validators.compose([Validators.required])]
      });
  
      this.projectService.getProjectTags().subscribe(data=>{
        this.projectTags = data.responseData.entities;
      });
  
      this.statuscodeService.getStatusCode().subscribe(data=>{
        this.stausCodes = data.responseData.entities;
      });
    }

  ngOnInit() {
    
    setTimeout(() => {
      if(undefined != localStorage.getItem(environment.PROJECT_CODE)){
        
        this.user.controls['project'].patchValue(localStorage.getItem(environment.PROJECT_CODE));
        let stausCode = localStorage.getItem(environment.STATUS_CODE);
        let tmp = stausCode.split(',');
        tmp.forEach(element => {
          this.arr.push(element);
        });
        
        this.user.controls['statuscode'].patchValue(this.arr);
      }
    }, 600);
   
  }

  async onSubmit(formData) {
    this.settingService.saveSetting(formData).subscribe(async res => {
      console.log(res);
      let alert = await this.alertCtrl.create({
        message: 'Setting saved',
        buttons: [{
          text: 'OK',
          handler: data => {
            this.router.navigateByUrl("home");
          }
        }]
      });
      alert.present();
    }, async error => {
      let alert = await this.alertCtrl.create({
        message: 'Error Occured',
        buttons: [{
          text: 'OK',
          handler: data => {
            this.router.navigateByUrl("dome");
          }
        }]
      });
      alert.present();

    });
  }

}
