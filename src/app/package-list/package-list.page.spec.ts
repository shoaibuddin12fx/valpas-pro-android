import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PackageListPage } from './package-list.page';

describe('PackageListPage', () => {
  let component: PackageListPage;
  let fixture: ComponentFixture<PackageListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PackageListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
