import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookContainerPageRoutingModule } from './book-container-routing.module';

import { BookContainerPage } from './book-container.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    BookContainerPageRoutingModule
  ],
  declarations: [BookContainerPage]
})
export class BookContainerPageModule {}
