import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookContainerPage } from './book-container.page';

const routes: Routes = [
  {
    path: '',
    component: BookContainerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookContainerPageRoutingModule {}
