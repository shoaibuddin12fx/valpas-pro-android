
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";

import { AppSettings } from './services/app-settings';
import { ToastService } from './services/toast-service';
import { LoadingService } from './services/loading-service';

import { HttpModule } from '@angular/http';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { IonicStorageModule } from '@ionic/storage';
import { CommonService } from './services/common.service';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { TranslationService } from './services/translation-service.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './services/api.service';
import { EventsService } from './services/events.service';
import { NetworkService } from './services/network.service';
import { SqliteService } from './services/sqlite.service';
import { StorageService } from './services/storage.service';
import { InterceptorService } from './services/interceptor.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GeolocationsService } from './services/geolocations.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { QrcodeService } from './services/qrcode.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { BobjectsService } from './services/bobjects.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { NgCalendarModule  } from 'ionic2-calendar';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeFi from '@angular/common/locales/fi';
import localeSv from '@angular/common/locales/sv';
import localeEn from '@angular/common/locales/en';
registerLocaleData(localeDe)
registerLocaleData(localeFi)
registerLocaleData(localeSv)
registerLocaleData(localeEn)
import { AppErrorHandler } from './AppErrorHandler';
// import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { CallNumber } from '@ionic-native/call-number/ngx';
import { File } from '@ionic-native/file/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    AngularFireModule.initializeApp(AppSettings.FIREBASE_CONFIG),
    AngularFireDatabaseModule, AngularFireAuthModule,
    BrowserModule, HttpModule, HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    TranslateModule.forRoot({ 
      loader: {  
        provide: TranslateLoader, 
        useFactory: (createTranslateLoader),  
        deps: [HttpClient] 
      } 
    }), // end translate module
    NgCalendarModule,
    AutocompleteLibModule
  ],
  providers: [
     BarcodeScanner,
     ToastService, LoadingService,CommonService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: ErrorHandler, useClass: AppErrorHandler },
    Geolocation,
    LocationAccuracy,
    LaunchNavigator,
    NgxPubSubService,
    NativeStorage,
    TranslationService,
    EventsService,
    SQLite,
    SqliteService,
    StorageService,
    ApiService,
    NetworkService,
    GeolocationsService,
    QrcodeService,
    BobjectsService,
    AndroidPermissions,
    // FirebaseX,
    CallNumber,
    File,
    Camera,
    InAppBrowser,
    PhotoViewer
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
