import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ValpasProAndroid',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
